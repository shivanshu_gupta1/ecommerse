import React from 'react'
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import CompareIcon from '@material-ui/icons/Compare';
import StarsIcon from '@material-ui/icons/Stars';
import FavoriteIcon from '@material-ui/icons/Favorite';

const Navigation = () => {
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [openSignUp, setOpenSignUp] = React.useState(false);
  const handleClickOpenSignUp = () => {
    setOpenSignUp(true);
  };
  const handleCloseSignUp = () => {
    setOpenSignUp(false);
  };

  const registerOpen = option => {
    if ( option === "Register" ) {
      handleClose();
      handleClickOpenSignUp();
    } else {
      handleCloseSignUp();
      handleClickOpen();
    }
  }

  return (
    <React.Fragment>

      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Login</DialogTitle>
        <DialogContent>
          <DialogContentText style={{ color: '#000000' }}>
            Please Logged In to this website, your email and password are end to end encrypted and secure.
          </DialogContentText>
          <TextField style={{ marginTop: '4px' }} id="outlined-basic" label="Email" type="email" variant="outlined" fullWidth />
          <br />
          <TextField style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Password" type="password" variant="outlined" fullWidth />
          
          <Button style={{ marginTop: '14px', marginBottom: '10px' }} size="large" startIcon={<ExitToAppIcon />} style={{ backgroundColor: '#000000', color: '#ffffff' }} variant="outlined" fullWidth >Login</Button>
          <center style={{ marginTop: '10px' }}>
            OR
          </center>
          <Button style={{ backgroundColor: '#ffffff', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
              <img alt="DocsUp" src="https://img.icons8.com/color/28/000000/google-logo.png"/>
              Continue with Google
          </Button>
          <Button style={{ backgroundColor: '#ffffff', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
              <img alt="DocsUp" src="https://img.icons8.com/color/28/000000/facebook-new.png"/>
              Continue with Facebook
          </Button>
          <Button style={{ backgroundColor: '#ffffff', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
            <img src="https://img.icons8.com/cute-clipart/28/000000/instagram-new.png"/>
              Continue with Instagram
          </Button>

          
          <center style={{ marginTop: '20px', marginBottom: '10px' }}>
            <a onClick={() => registerOpen('Register')} href="#!">
              Don't have an Account Sign Up
            </a>
          </center>

        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog open={openSignUp} onClose={handleCloseSignUp} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Sign Up</DialogTitle>
        <DialogContent>
          <DialogContentText style={{ color: '#000000' }}>
            Please Register into this website, your email and password are end to end encrypted and secure.
          </DialogContentText>
          <TextField style={{ marginTop: '4px' }} id="outlined-basic" label="Full Name" type="text" variant="outlined" fullWidth />
          <br />
          <TextField style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Email" type="email" variant="outlined" fullWidth />
          <TextField style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Password" type="password" variant="outlined" fullWidth />
          <TextField style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Confirm Password" type="password" variant="outlined" fullWidth />
          
          <Button style={{ marginTop: '14px', marginBottom: '10px' }} size="large" startIcon={<ExitToAppIcon />} style={{ backgroundColor: '#000000', color: '#ffffff' }} variant="outlined" fullWidth >Sign Up</Button>
          
          <center style={{ marginTop: '20px', marginBottom: '10px' }}>
            <a onClick={() => registerOpen('Login')} href="#!">
              Already have an account
            </a>
          </center>

        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseSignUp} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>



      <div className = "main-wrapper">
        <nav className = "navbar">
          <div className = "brand-and-icon">
            <a href = "index.html" className = "navbar-brand">MYWEBSITE</a>
            <button type = "button" className = "navbar-toggler">
              <i className = "fas fa-bars"></i>
            </button>
          </div>
          <div className = "navbar-collapse">
            <ul className = "navbar-nav">
              <li>
                <a href = "/">home</a>
              </li>

              <li>
                <a href = "#" className = "menu-link">
                  electronics
                  <span className = "drop-icon">
                    <i className = "fas fa-chevron-down"></i>
                  </span>
                </a>
                <div className = "sub-menu">
                  <div className = "sub-menu-item">
                    <h4>top categories</h4>
                    <ul>
                      <li><a href = "#">cell phones & accessories</a></li>
                      <li><a href = "#">smart tv</a></li>
                      <li><a href = "#">computer & laptops</a></li>
                      <li><a href = "#">digital cameras</a></li>
                      <li><a href = "#">video games & accessories</a></li>
                    </ul>
                  </div>
                  <div className = "sub-menu-item">
                    <h4>other categories</h4>
                    <ul>
                      <li><a href = "#">iphones</a></li>
                      <li><a href = "#">speakers</a></li>
                      <li><a href = "#">samsung devices</a></li>
                      <li><a href = "#">audio & headphones</a></li>
                      <li><a href = "#">vehicles electronics & GPS</a></li>
                    </ul>
                  </div>
                  {/* <div className = "sub-menu-item">
                    <h2>all essential devices and tools for home</h2>
                    <button type = "button" className = "btn">shop here</button>
                  </div> */}
                  <div className = "sub-menu-item">
                    <img style={{ width: '75%' }} src = "https://images-na.ssl-images-amazon.com/images/I/613SYKy-XPL._UL1200_.jpg" alt = "product image" />
                  </div>
                </div>
              </li>

              <li>
                <a href = "#" className = "menu-link">
                  fashion
                  <span className = "drop-icon">
                    <i className = "fas fa-chevron-down"></i>
                  </span>
                </a>
                <div className = "sub-menu">
                  <div className = "sub-menu-item">
                    <h4>top categories</h4>
                    <ul>
                      <li><a href = "#">men's clothing</a></li>
                      <li><a href = "#">women's clothing</a></li>
                      <li><a href = "#">men's shoes</a></li>
                      <li><a href = "#">women's shoes</a></li>
                      <li><a href = "#">clothing deals</a></li>
                    </ul>
                  </div>
                  <div className = "sub-menu-item">
                    <h4>other categories</h4>
                    <ul>
                      <li><a href = "#">fine jewelry</a></li>
                      <li><a href = "#">fashion jewelry</a></li>
                      <li><a href = "#">men's accessories</a></li>
                      <li><a href = "#">handbags & bags</a></li>
                      <li><a href = "#">kid's clothing</a></li>
                    </ul>
                  </div>
                  {/* <div className = "sub-menu-item">
                    <h2>stylish and modern fashion clothing</h2>
                    <button type = "button" className = "btn">shop here</button>
                  </div> */}
                  <div className = "sub-menu-item">
                    <img style={{ width: '75%' }} src = "https://images-na.ssl-images-amazon.com/images/I/613SYKy-XPL._UL1200_.jpg" alt = "product image" />
                  </div>
                </div>
              </li>

              <li>
                <a href = "#" className = "menu-link">
                  health & beauty
                  <span className = "drop-icon">
                    <i className = "fas fa-chevron-down"></i>
                  </span>
                </a>
                <div className = "sub-menu">
                  <div className = "sub-menu-item">
                    <h4>top categories</h4>
                    <ul>
                      <li><a href = "#">makeup</a></li>
                      <li><a href = "#">health care</a></li>
                      <li><a href = "#">fragrance</a></li>
                      <li><a href = "#">hair care & stylings</a></li>
                      <li><a href = "#">manicure & pedicure</a></li>
                    </ul>
                  </div>
                  <div className = "sub-menu-item">
                    <h4>other categories</h4>
                    <ul>
                      <li><a href = "#">skin care</a></li>
                      <li><a href = "#">vitamins</a></li>
                      <li><a href = "#">vision care</a></li>
                      <li><a href = "#">oral care</a></li>
                      <li><a href = "#">shaving & hair removal</a></li>
                    </ul>
                  </div>
                  {/* <div className = "sub-menu-item">
                    <h2>the latest product is here</h2>
                    <button type = "button" className = "btn">shop here</button>
                  </div> */}
                  <div className = "sub-menu-item">
                    <img style={{ width: '75%' }} src = "https://images-na.ssl-images-amazon.com/images/I/613SYKy-XPL._UL1200_.jpg" alt = "product image" />
                  </div>
                </div>
              </li>

              <li>
                <a href = "#" className = "menu-link">
                  sports
                  <span className = "drop-icon">
                    <i className = "fas fa-chevron-down"></i>
                  </span>
                </a>
                <div className = "sub-menu">
                  <div className = "sub-menu-item">
                    <h4>top categories</h4>
                    <ul>
                      <li><a href = "#">cycling</a></li>
                      <li><a href = "#">outdoor sports</a></li>
                      <li><a href = "#">hunting</a></li>
                      <li><a href = "#">fishing</a></li>
                      <li><a href = "#">fitness & yoga</a></li>
                    </ul>
                  </div>
                  <div className = "sub-menu-item">
                    <h4>other categories</h4>
                    <ul>
                      <li><a href = "#">tennis</a></li>
                      <li><a href = "#">swimming</a></li>
                      <li><a href = "#">winter sports</a></li>
                      <li><a href = "#">fitness technology</a></li>
                      <li><a href = "#">sports wear</a></li>
                    </ul>
                  </div>
                  {/* <div className = "sub-menu-item">
                    <h2>gear up for sports & adventures</h2>
                    <button type = "button" className = "btn">shop here</button>
                  </div> */}
                  <div className = "sub-menu-item">
                    <img style={{ width: '75%' }} src = "https://images-na.ssl-images-amazon.com/images/I/613SYKy-XPL._UL1200_.jpg" alt = "product image" />
                  </div>
                </div>
              </li>

              <li>
              <Badge badgeContent={0} color="secondary">
                <IconButton href="/compare" aria-label="delete">
                  <CompareIcon />
                </IconButton>
              </Badge>
              </li>
              <li>
              <IconButton onClick={() => window.location.href = "/wishlist"} aria-label="delete">
                  <FavoriteIcon style={{ color: '#000' }} />
              </IconButton>
              </li>
              <li>
              <IconButton onClick={handleClickOpen} aria-label="delete">
                  <ExitToAppIcon style={{ color: '#000' }} />
              </IconButton>
              </li>
            </ul>
          </div>
        </nav>
    </div>
    </React.Fragment>
  )
}

export default Navigation
