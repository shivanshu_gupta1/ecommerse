import React from 'react'
import Navigation from '../../static/Navigation';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import LinearProgress from '@material-ui/core/LinearProgress';

import {
    BrowserView,
    MobileView
} from "react-device-detect";

const useStyles = makeStyles((theme) => ({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: 600,
      marginTop: '10px'
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
        color: '#000000',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: '3vh'
    },
    cardroot: {
        minWidth: 285,
    },
}));

const WishList = () => {
    const classes = useStyles();
    return (
        <div>
        <CssBaseline />
            <Navigation />
            <center>
                <Paper component="form" className={classes.root}>
                    <InputBase
                        className={classes.input}
                        placeholder="Search Products"
                        inputProps={{ 'aria-label': 'search products' }}
                    />
                    <IconButton type="submit" className={classes.iconButton} aria-label="search">
                        <SearchIcon />
                    </IconButton>
                </Paper>
            </center>
                <BrowserView>
                    <Container style={{ marginTop: '20px' }}>
                    <Grid container spacing={3}>
                        <Grid item xs={3}>
                            <Card className={classes.cardroot} variant="outlined">
                                <CardContent>
                                    <img style={{ float: 'right' }} src="https://img.icons8.com/color/28/000000/filled-like.png"/>
                                    <br />
                                    <br />
                                    <center>
                                        <img style={{ width: '30vh' }} src = "https://images-na.ssl-images-amazon.com/images/I/613SYKy-XPL._UL1200_.jpg" alt = "product image" />
                                    </center>
                                    <Typography className={classes.pos} >
                                    adjective
                                    </Typography>
                                    <center>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <p>
                                            (4/5)
                                        </p>
                                    </center>
                                    <Typography style={{ textAlign: 'center', marginTop: '4px' }} variant="body2" component="p">
                                        some random text format is here for the product
                                    </Typography>
                                </CardContent>
                                <div style={{ padding: '4px' }}>
                                    <Button variant="outlined" fullWidth >Add to Compare</Button>
                                </div>
                            </Card>
                        </Grid>
                        <Grid item xs={3}>
                            <Card className={classes.cardroot} variant="outlined">
                                <CardContent>
                                    <img style={{ float: 'right' }} src="https://img.icons8.com/color/28/000000/filled-like.png"/>
                                    <br />
                                    <br />
                                    <center>
                                        <img style={{ width: '30vh' }} src = "https://images-na.ssl-images-amazon.com/images/I/613SYKy-XPL._UL1200_.jpg" alt = "product image" />
                                    </center>
                                    <Typography className={classes.pos} >
                                    adjective
                                    </Typography>
                                    <center>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <p>
                                            (4/5)
                                        </p>
                                    </center>
                                    <Typography style={{ textAlign: 'center', marginTop: '4px' }} variant="body2" component="p">
                                        some random text format is here for the product
                                    </Typography>
                                </CardContent>
                                <div style={{ padding: '4px' }}>
                                    <Button variant="outlined" fullWidth >Add to Compare</Button>
                                </div>
                            </Card>
                        </Grid>
                        <Grid item xs={3}>
                            <Card className={classes.cardroot} variant="outlined">
                                <CardContent>
                                    <img style={{ float: 'right' }} src="https://img.icons8.com/color/28/000000/filled-like.png"/>
                                    <br />
                                    <br />
                                    <center>
                                        <img style={{ width: '30vh' }} src = "https://images-na.ssl-images-amazon.com/images/I/613SYKy-XPL._UL1200_.jpg" alt = "product image" />
                                    </center>
                                    <Typography className={classes.pos} >
                                    adjective
                                    </Typography>
                                    <center>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <p>
                                            (4/5)
                                        </p>
                                    </center>
                                    <Typography style={{ textAlign: 'center', marginTop: '4px' }} variant="body2" component="p">
                                        some random text format is here for the product
                                    </Typography>
                                </CardContent>
                                <div style={{ padding: '4px' }}>
                                    <Button variant="outlined" fullWidth >Add to Compare</Button>
                                </div>
                            </Card>
                        </Grid>
                        <Grid item xs={3}>
                            <Card className={classes.cardroot} variant="outlined">
                                <CardContent>
                                    <img style={{ float: 'right' }} src="https://img.icons8.com/color/28/000000/filled-like.png"/>
                                    <br />
                                    <br />
                                    <center>
                                        <img style={{ width: '30vh' }} src = "https://images-na.ssl-images-amazon.com/images/I/613SYKy-XPL._UL1200_.jpg" alt = "product image" />
                                    </center>
                                    <Typography className={classes.pos} >
                                    adjective
                                    </Typography>
                                    <center>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <img src="https://img.icons8.com/fluent/28/000000/star.png"/>
                                        <p>
                                            (4/5)
                                        </p>
                                    </center>
                                    <Typography style={{ textAlign: 'center', marginTop: '4px' }} variant="body2" component="p">
                                        some random text format is here for the product
                                    </Typography>
                                </CardContent>
                                <div style={{ padding: '4px' }}>
                                    <Button variant="outlined" fullWidth >Add to Compare</Button>
                                </div>
                            </Card>
                        </Grid>
                    </Grid>
                    </Container>
                </BrowserView>
        </div>
    )
}

export default WishList;
