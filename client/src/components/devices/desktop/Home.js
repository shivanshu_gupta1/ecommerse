import React from 'react'
import Navigation from '../../static/Navigation';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Splide, SplideSlide } from '@splidejs/react-splide';                 
import '@splidejs/splide/dist/css/themes/splide-default.min.css';
import FilterListIcon from '@material-ui/icons/FilterList';
import Collapse from '@material-ui/core/Collapse';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Slider from '@material-ui/core/Slider';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import axios from 'axios';
import CryptoJS from 'crypto';
import Footer from '../../static/Footer';
// API Service
import { API_SERVICE, SECRET_KEY } from '../../../config/URI';

const ex3 = {
    'width': '100%',
    'height': '125px',
    'overflow': 'auto'
}

const useStyles = makeStyles((theme) => ({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: 600,
      marginTop: '10px'
    },
    priceroot: {
        width: 300,
    },
    slideroot: {
        margin: '10px'
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
        color: '#000000',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: '3vh'
    },
    cardroot: {
        minWidth: 230,
    },
    gridroot: {
        flexGrow: 1,
    },
}));

function valuetext(value) {
    return `${value}°C`;
}


const ProductList = ({ product }) => {
    const classes = useStyles();
    var mykey = CryptoJS.createCipher('aes-128-cbc', SECRET_KEY);
    var eE = mykey.update(product._id, 'utf8', 'hex');
    eE += mykey.final('hex');


    return (
        <Grid item xs={3}>
            <Card className={classes.cardroot} variant="outlined">
                <CardContent>
                    <center>
                        <a href={`/product?i=${eE}`}>
                        <img style={{ width: '15vh' }} src = {product.photo[0]} alt = "product image" />
                        </a>
                    </center>
                    <Typography className={classes.pos} >
                    {product.name}
                    </Typography>
                    <Typography style={{ textAlign: 'center' }} variant="body2" component="p">
                        {product.about}
                    </Typography>
                </CardContent>
                <div style={{ padding: '4px' }}>
                    <Button variant="outlined" fullWidth >Add to Compare</Button>
                    <br />
                    <Button style={{ marginTop: '2px', backgroundColor: '#000000', color: '#ffffff' }} variant="outlined" fullWidth >Add to Wishlist</Button>
                </div>
            </Card>
        </Grid>
    )
}

const Home = () => {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const [value, setValue] = React.useState([0, 37]);
    const [products, setProducts] = React.useState([]);

    React.useEffect(() => {
        axios.get(`${API_SERVICE}/api/v1/main/products`)
        .then(response => {
            setProducts(response.data);
        })
        .catch(err => console.log(err))
    }, [])

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    const primaryOptions = {
        type      : 'loop',
        height     : 350,
        perPage   : 1,
        perMove   : 1,
        gap       : '1rem',
        autoplay     : true,
        pagination: false,
    };

    const [state, setState] = React.useState({
        checkedA: false,
        checkedB: false,
        checkedF: false,
        checkedG: false,
    });

    const handleChangeOptions = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };

    const showProductList = () => {
        return products.map(product => {
            return <ProductList product={product} key={product._id} />
        })
    }

    return (
        <div>
        <CssBaseline />
            <Navigation />
            <center>
                <Paper component="form" className={classes.root}>
                    <InputBase
                        className={classes.input}
                        placeholder="Search Products"
                        inputProps={{ 'aria-label': 'search products' }}
                    />
                    <IconButton type="submit" className={classes.iconButton} aria-label="search">
                        <SearchIcon />
                    </IconButton>
                </Paper>
                <Paper className={classes.slideroot}>
                    <Splide options={ primaryOptions }>
                        <SplideSlide>
                            <img width="100%" src="https://www.samuel-windsor.co.uk/images/73/choose-formal-shirt-header.jpg" alt="Image 1"/>
                        </SplideSlide>
                        <SplideSlide>
                            <img width="100%" src="https://www.samuel-windsor.co.uk/images/73/choose-formal-shirt-header.jpg" alt="Image 2"/>
                        </SplideSlide>
                    </Splide>
                </Paper>
                
            </center>
                <Container style={{ marginTop: '20px' }}>
                    {/* <Button 
                        onClick={handleExpandClick}
                        aria-expanded={expanded} 
                        style={{ marginBottom: '2px', marginTop: '2px' }} 
                        startIcon={<FilterListIcon />
                        }>
                        Filter
                    </Button> */}
                    
                    <Collapse style={{ margin: '2px 0px 2px 0px' }} in={expanded} timeout="auto" unmountOnExit>
                        <Card variant="outlined">
                            <CardContent>
                                <FormControl style={{ width: '120px' }}>
                                    <InputLabel id="demo-simple-select-label">Sort</InputLabel>
                                    <Select
                                    style={{ color: '#00000' }} 
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    >
                                    <MenuItem value="Our Favorites">Our Favorites</MenuItem>
                                    <MenuItem value="What's new">What's new</MenuItem>
                                    <MenuItem value="Price High to Low">Price High to Low</MenuItem>
                                    <MenuItem value="Price Low to High">Price Low to High</MenuItem>
                                    </Select>
                                </FormControl>

                                <FormControl style={{ width: '180px', marginLeft: '10px' }}>
                                    <InputLabel id="demo-simple-select-label">Responsible</InputLabel>
                                    <Select
                                    style={{ color: '#00000' }} 
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    >
                                    <MenuItem value="Recycled">Recycled</MenuItem>
                                    <MenuItem value="Sustinable Materials">Sustinable Materials</MenuItem>
                                    </Select>
                                </FormControl>

                                <FormControl style={{ width: '180px', marginLeft: '10px' }}>
                                    <InputLabel id="demo-simple-select-label">Gender</InputLabel>
                                    <Select
                                    style={{ color: '#00000' }} 
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    >
                                    <MenuItem value="Unisex">Unisex</MenuItem>
                                    <MenuItem value="Male">Male</MenuItem>
                                    <MenuItem value="Female">Female</MenuItem>
                                    </Select>
                                </FormControl>

                                <FormControl style={{ width: '180px', marginLeft: '10px' }}>
                                    <InputLabel id="demo-simple-select-label">Brand</InputLabel>
                                    <Select
                                    style={{ color: '#00000' }} 
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    >
                                    <MenuItem value="Addidas">Addidas</MenuItem>
                                    <MenuItem value="Nike">Nike</MenuItem>
                                    </Select>
                                </FormControl>

                                <FormControl style={{ width: '180px', marginLeft: '10px' }}>
                                    <InputLabel id="demo-simple-select-label">Product Type</InputLabel>
                                    <Select
                                    style={{ color: '#00000' }} 
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    >
                                    <MenuItem value="Bagpacks">Bagpacks</MenuItem>
                                    <MenuItem value="Bags">Bags</MenuItem>
                                    <MenuItem value="Bum Bags">Bum Bags</MenuItem>
                                    <MenuItem value="Face Covering">Face Covering</MenuItem>
                                    <MenuItem value="Gilets">Gilets</MenuItem>
                                    </Select>
                                </FormControl>

                                <FormControl style={{ width: '100px', marginLeft: '10px' }}>
                                    <InputLabel id="demo-simple-select-label">Size</InputLabel>
                                    <Select
                                    style={{ color: '#00000' }} 
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    >
                                    <MenuItem value="UK 4">UK 4</MenuItem>
                                    <MenuItem value="UK 4.5">UK 4.5</MenuItem>
                                    <MenuItem value="UK 6">UK 6</MenuItem>
                                    <MenuItem value="UK 7">UK 7</MenuItem>
                                    <MenuItem value="UK 7.5">UK 7.5</MenuItem>
                                    </Select>
                                </FormControl>

                                <FormControl style={{ width: '180px', marginLeft: '10px' }}>
                                    <InputLabel id="demo-simple-select-label">Colors</InputLabel>
                                    <Select
                                    style={{ color: '#00000' }} 
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    >
                                    <MenuItem value="White">White</MenuItem>
                                    <MenuItem value="Black">Black</MenuItem>
                                    <MenuItem value="Brown">Brown</MenuItem>
                                    <MenuItem value="Red">Red</MenuItem>
                                    <MenuItem value="Green">Green</MenuItem>
                                    </Select>
                                </FormControl>

                                <FormControl style={{ width: '140px', marginLeft: '10px' }}>
                                    <InputLabel id="demo-simple-select-label">Body Fits</InputLabel>
                                    <Select
                                    style={{ color: '#00000' }} 
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    >
                                    <MenuItem value="Main Collection">Main Collection</MenuItem>
                                    </Select>
                                </FormControl>

                                <FormControl style={{ width: '180px', marginLeft: '10px' }}>
                                    <InputLabel id="demo-simple-select-label">Sale / New Season</InputLabel>
                                    <Select
                                    style={{ color: '#00000' }} 
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    >
                                    <MenuItem value="New Season">New Season</MenuItem>
                                    <MenuItem value="Sale">Sale</MenuItem>
                                    </Select>
                                </FormControl>
                            </CardContent>
                        </Card>
                    </Collapse>
                    
                    <div className={classes.gridroot}>
                        <Grid container spacing={3}>
                            <Grid item xs={3}>
                                <Card style={{ minWidth: '100%', padding: '6px' }} variant="outlined">
                                    <h3>FILTER BY</h3>
                                    <hr style={{ borderTop: '3px solid #000000', marginTop: '6px', marginBottom: '6px' }} />
                                    <h4>Product Type</h4>

                                    <div style={ex3}>
                                        <FormControlLabel
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}
                                            />
                                            }
                                            label="BODYCON"
                                        />
                                        <br />
                                        <FormControlLabel
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="BODYSUIT"
                                        />
                                        <br />
                                        <FormControlLabel
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="BODYSUIT"
                                        />
                                        <br />
                                        <FormControlLabel
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="BODYSUIT"
                                        />
                                        <br />
                                        <FormControlLabel
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="BODYSUIT"
                                        />
                                        <br />
                                        <FormControlLabel
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="BODYSUIT"
                                        />
                                        
                                         
                                    </div>

                                    <hr style={{ borderTop: '3px solid #000000', marginTop: '4px', marginBottom: '4px' }} />
                                    <h4>Colour</h4>
                                    <div style={ex3}>
                                        <FormControlLabel
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="BLACK"
                                        />
                                        <FormControlLabel
                                            style={{ marginLeft: '-2px' }}
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="NUDE"
                                        />
                                        <FormControlLabel
                                            style={{ float: 'left' }}
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="BLUE"
                                        />
                                        <FormControlLabel
                                            style={{ float: 'left', marginLeft: '9px' }}
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="ORANGE"
                                        />
                                        <FormControlLabel
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="BROWN"
                                        />
                                        <FormControlLabel
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="PINK"
                                        />
                                        <FormControlLabel
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="CREAM"
                                        />
                                        <FormControlLabel
                                            style={{ marginLeft: '-8px' }}
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="PURPLE"
                                        />
                                        <FormControlLabel
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="GREEN"
                                        />
                                        <FormControlLabel
                                            style={{ marginLeft: '-6px' }}
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="RED"
                                        />
                                        <FormControlLabel
                                            style={{ marginLeft: '-4px' }}
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="GREY"
                                        />
                                        <FormControlLabel
                                            style={{ float: 'left' }}
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="WHITE"
                                        />
                                        <FormControlLabel
                                            style={{ float: 'left' }}
                                            control={
                                            <Checkbox
                                                checked={state.checkedB}
                                                onChange={handleChangeOptions}
                                                name="checkedB"
                                                style={{ color: '#000000' }}

                                            />
                                            }
                                            label="WHITE"
                                        />
                                    </div>

                                    <hr style={{ borderTop: '3px solid #000000', marginTop: '4px', marginBottom: '4px' }} />
                                    <h4>Clothes Size</h4>
                                    <div style={ex3}>
                                        <table>
                                            <tr>
                                                <th style={{ float: 'left' }}>
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={state.checkedB}
                                                            onChange={handleChangeOptions}
                                                            name="checkedB"
                                                            style={{ color: '#000000' }}

                                                        />
                                                        }
                                                        label="6"
                                                    />
                                                </th>
                                                <th>
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={state.checkedB}
                                                            onChange={handleChangeOptions}
                                                            name="checkedB"
                                                            style={{ color: '#000000' }}

                                                        />
                                                        }
                                                        label="14"
                                                    />
                                                </th>
                                            </tr>

                                            <tr>
                                                <th style={{ float: 'left' }}>
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={state.checkedB}
                                                            onChange={handleChangeOptions}
                                                            name="checkedB"
                                                            style={{ color: '#000000' }}

                                                        />
                                                        }
                                                        label="8"
                                                    />
                                                </th>
                                                <th>
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={state.checkedB}
                                                            onChange={handleChangeOptions}
                                                            name="checkedB"
                                                            style={{ color: '#000000' }}

                                                        />
                                                        }
                                                        label="16"
                                                    />
                                                </th>
                                            </tr>
                                            
                                            <tr>
                                                <th style={{ float: 'left' }}>
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={state.checkedB}
                                                            onChange={handleChangeOptions}
                                                            name="checkedB"
                                                            style={{ color: '#000000' }}

                                                        />
                                                        }
                                                        label="10"
                                                    />
                                                </th>
                                                <th>
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={state.checkedB}
                                                            onChange={handleChangeOptions}
                                                            name="checkedB"
                                                            style={{ color: '#000000' }}

                                                        />
                                                        }
                                                        label="18"
                                                    />
                                                </th>
                                            </tr>

                                            <tr>
                                                <th style={{ float: 'left' }}>
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={state.checkedB}
                                                            onChange={handleChangeOptions}
                                                            name="checkedB"
                                                            style={{ color: '#000000' }}

                                                        />
                                                        }
                                                        label="12"
                                                    />
                                                </th>
                                                <th>
                                                    <FormControlLabel
                                                        control={
                                                        <Checkbox
                                                            checked={state.checkedB}
                                                            onChange={handleChangeOptions}
                                                            name="checkedB"
                                                            style={{ color: '#000000' }}

                                                        />
                                                        }
                                                        label="20"
                                                    />
                                                </th>
                                            </tr>
                                        </table>
                                    </div>

                                    <hr style={{ borderTop: '3px solid #000000', marginTop: '4px', marginBottom: '4px' }} />
                                    <h4>Price</h4>
                                    <div style={{ marginTop: '12px', marginRight: '4px', marginLeft: '4px' }}>
                                        <Slider
                                            value={value}
                                            onChange={handleChange}
                                            valueLabelDisplay="auto"
                                            aria-labelledby="range-slider"
                                            getAriaValueText={valuetext}
                                            style={{ color: '#000000' }}
                                        />
                                    </div>
                                </Card>
                            </Grid>
                            <Grid item xs={9}>
                                <Grid container spacing={2}>
                                    {showProductList()}
                                </Grid>
                            </Grid>
                        </Grid>
                    </div>
                </Container>
                <Footer />
        </div>
    )
}

export default Home
