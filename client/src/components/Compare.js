import React from 'react';
import {
    BrowserView,
    MobileView
} from "react-device-detect";
import DesktopCompare from './devices/desktop/Compare';
import MobileCompare from './devices/mobile/Compare';

const Compare = () => {
    return (
        <>
            <BrowserView>
                <DesktopCompare />
            </BrowserView>
            <MobileView>
                <MobileCompare />
            </MobileView>
        </>
    )
}

export default Compare
