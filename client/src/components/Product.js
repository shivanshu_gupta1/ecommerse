import React from 'react';
import {
    BrowserView,
    MobileView
} from "react-device-detect";

import DesktopProduct from './devices/desktop/Product';
import MobileProduct from './devices/mobile/Product';
const Product = () => {
    return (
        <>
            <BrowserView>
                <DesktopProduct />
            </BrowserView>
            <MobileView>
                <MobileProduct />
            </MobileView>
        </>
    )
}

export default Product
