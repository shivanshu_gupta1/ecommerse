import React from 'react';
import {
    BrowserView,
    MobileView
} from "react-device-detect";
import DesktopHome from './devices/desktop/Home';
import MobileHome from './devices/mobile/Home';


const Home = () => {
    return (
        <>
            <BrowserView>
                <DesktopHome />
            </BrowserView>
            <MobileView>
                <MobileHome />
            </MobileView>
        </>
    )
}

export default Home
