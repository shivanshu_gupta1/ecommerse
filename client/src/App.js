import React from "react";
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
// Components
import Home from './components/Home';
import Compare from './components/Compare';
import Product from './components/Product';
import Wishlist from './components/WishList';

function App() {
  return (
    <React.Fragment>
      <Router>
          <Route path="/" exact component={Home} />
          <Route path="/compare" exact component={Compare} />
          <Route path="/product" exact component={Product} />
          <Route path="/wishlist" exact component={Wishlist} />
        </Router>
    </React.Fragment>
  );
}

export default App;
